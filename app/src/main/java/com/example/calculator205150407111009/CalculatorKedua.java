package com.example.calculator205150407111009;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class CalculatorKedua extends AppCompatActivity {
   private TextView papanhasil,papan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator_kedua);

        papan = findViewById(R.id.papanhasil);
        String hasil = getIntent().getStringExtra("keyhasil");
        papan.setText(hasil);
    }
}