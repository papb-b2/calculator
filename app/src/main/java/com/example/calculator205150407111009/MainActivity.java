package com.example.calculator205150407111009;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView papan;
    TextView papan2;
    String inputString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        papan = findViewById(R.id.papan);
        papan2 = findViewById(R.id.papan2);

        Button b1 = findViewById(R.id.b1);
        Button b2 = findViewById(R.id.b2);
        Button b3 = findViewById(R.id.b3);
        Button b4 = findViewById(R.id.b4);
        Button b5 = findViewById(R.id.b5);
        Button b6 = findViewById(R.id.b6);
        Button b7 = findViewById(R.id.b7);
        Button b8 = findViewById(R.id.b8);
        Button b9 = findViewById(R.id.b9);
        Button b0 = findViewById(R.id.b0);

        Button b_multiply = findViewById(R.id.bmultiply);
        Button b_divide = findViewById(R.id.bdivide);
        Button b_minus = findViewById(R.id.bminus);
        Button b_plus = findViewById(R.id.bplus);

        Button b_equal = findViewById(R.id.bequal);

        inputString = "";

        b0.setOnClickListener(this);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
        b6.setOnClickListener(this);
        b7.setOnClickListener(this);
        b8.setOnClickListener(this);
        b9.setOnClickListener(this);

        b_multiply.setOnClickListener(this);
        b_divide.setOnClickListener(this);
        b_minus.setOnClickListener(this);
        b_plus.setOnClickListener(this);
        b_equal.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.b0:
                this.updateDisplay2("0");
                inputString += "0";
                break;

            case R.id.b1:
                this.updateDisplay2("1");
                inputString += "1";
                break;

            case R.id.b2:
                this.updateDisplay2("2");
                inputString += "2";
                break;

            case R.id.b3:
                this.updateDisplay2("3");
                inputString += "3";
                break;

            case R.id.b4:
                this.updateDisplay2("4");
                inputString += "4";
                break;

            case R.id.b5:
                this.updateDisplay2("5");
                inputString += "5";
                break;

            case R.id.b6:
                this.updateDisplay2("6");
                inputString += "6";
                break;

            case R.id.b7:
                this.updateDisplay2("7");
                inputString += "7";
                break;

            case R.id.b8:
                this.updateDisplay2("8");
                inputString += "8";
                break;

            case R.id.b9:
                this.updateDisplay2("9");
                inputString += "9";
                break;

            case R.id.bdivide:
                this.updateDisplay2("/");
                inputString += "/";
                break;

            case R.id.bmultiply:
                this.updateDisplay2("*");
                inputString += "*";
                break;

            case R.id.bminus:
                this.updateDisplay2("-");
                inputString += "-";
                break;

            case R.id.bplus:
                this.updateDisplay2("+");
                inputString += "+";
                break;

            case R.id.bequal:
                papan2.setText("");
                prepareinput(inputString);
                inputString = "";
                String hasil = papan.getText().toString();
                Intent pindahactivity =  new Intent(MainActivity.this, CalculatorKedua.class);
                pindahactivity.putExtra("keyhasil", hasil);
                startActivity(pindahactivity);
                break;
        }
    }

    private void updateDisplay(String value) {
        this.papan.setText(value);
    }

    private void updateDisplay2(final String value) {
        this.papan2.append(value);
    }

    private void prepareinput(String input) {
        if (input.startsWith(" ")) {
            String firstChar = String.valueOf(input.charAt(1));
            if (firstChar.contains("/") || firstChar.contains("*")) {
                updateDisplay("Error");
            } else {
                if (firstChar.contains("+")) {
                    input = input.replace(" + ", "+");
                }
                if (firstChar.contains("-")) {
                    input = input.replace(" - ", "-");
                }
            }
        }
        calculate(input);
    }

    private void calculate(String input) {
        String[] arr = input.split(" ");

        if (arr.length > 1) {
            for (int i = 0; i < arr.length; i++) {
                int temp = 0 ;
                try {
                    if (arr[i].contains("/")) {
                        int op1 = Integer.parseInt(arr[i-1]);
                        int op2 = Integer.parseInt(arr[i+1]);

                        if (op2 == 0) {
                            Toast.makeText(this, "Not Allowed", Toast.LENGTH_SHORT).show();
                            updateDisplay("Error");
                            inputString = "";
                            return;
                        }
                        temp = op1 / op2;
                        arr[i-1] = " ";
                        arr[i] = String.valueOf(temp);
                        arr[i+1] = " ";
                    }
                    if (arr[i].contains("*")) {
                        int op1 = Integer.parseInt(arr[i-1]);
                        int op2 = Integer.parseInt(arr[i+1]);

                        if (op2 == 0) {
                            Toast.makeText(this, "Not Allowed", Toast.LENGTH_SHORT).show();
                            updateDisplay("Error");
                            inputString = "";
                            return;
                        }
                        temp = op1 * op2;
                        arr[i-1] = " ";
                        arr[i] = String.valueOf(temp);
                        arr[i+1] = " ";
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            for (int i = 0; i < arr.length; i++) {
                int temp = 0 ;
                try {
                    if (arr[i].contains("+")) {
                        int op1 = Integer.parseInt(arr[i- 1]);
                        int op2 = Integer.parseInt(arr[i + 1]);

                        if (op2 == 0) {
                            Toast.makeText(this, "Not Allowed", Toast.LENGTH_SHORT).show();
                            updateDisplay("Error");
                            inputString = "";
                            return;
                        }
                        temp = op1 + op2;
                        arr[i-1] = " ";
                        arr[i] = String.valueOf(temp);
                        arr[i+1] = " ";
                    }
                    if (arr[i].contains("-")) {
                        int op1 = Integer.parseInt(arr[i - 1]);
                        int op2 = Integer.parseInt(arr[i + 1]);

                        if (op2 == 0) {
                            Toast.makeText(this, "Not Allowed", Toast.LENGTH_SHORT).show();
                            updateDisplay("Error");
                            inputString = "";
                            return;
                        }
                        temp = op1 - op2;
                        arr[i-1] = " ";
                        arr[i] = String.valueOf(temp);
                        arr[i+1] = " ";
                    }
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            inputString = " ";
            for (String s : arr){
                if (s != " "){
                    inputString += s;
                    inputString += " ";
                }
            }
            calculate(inputString);
        }
        else {
            updateDisplay(arr[0]);
        }
    }
}
